for i in {0..7}
do
    echo "Send start signal for team $i"
    ign topic -m ignition.msgs.Empty -t /model/vehicle_$i/start -p "{}" &
done