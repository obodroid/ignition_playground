gnome-terminal -- roslaunch ignition_playground challenge.launch
sleep 2s
for i in {0..7}
do
    sleep 1s
    echo "Spawn vehicle for team $i"
    gnome-terminal -- roslaunch ignition_playground diff_drive.launch num:=$i
done